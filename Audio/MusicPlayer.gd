# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends AudioStreamPlayer

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export (Array, AudioStreamOGGVorbis) var tracks : Array = [
	preload("res://Audio/Music/Darry2Vance-Bagpipes.ogg"),
	preload("res://Audio/Music/ebunny-Heartland.ogg"),
	preload("res://Audio/Music/Russsound-Fantasy_Adventure.ogg"),
	preload("res://Audio/Music/Syrgi_Romeo-Ocean_Depths.ogg"),
	preload("res://Audio/Music/Syrgi_Romeo-The_Entrance_To_The_Lagoon.ogg")
]

#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------

var current_track : int = -1

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(tracks.size() > 1, "Need at least 2 tracks!")
	bus = "Music"
	pause_mode = Node.PAUSE_MODE_PROCESS
	volume_db = -20.0
	#print("Music-Player found %d tracks" % tracks.size())


func _input(event : InputEvent) -> void:
	#if event.is_action("master_mute"):
	#	WishAudioBus.set_mute("Master", not WishAudioBus.get_mute("Master"))
	if event.is_action("next_song"):
		stop()
		play_random_track()

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func play_random_track() -> void:
	stop()
	var old_track : int = current_track
	while current_track == old_track:
		current_track = randi() % tracks.size()
	stream = tracks[current_track]
	play()
		

#-[SIGNAL METHODS]--------------------------------------------------------------

func _on_MusikPlayer_finished() -> void:
	print("next_track")
	play_random_track()
