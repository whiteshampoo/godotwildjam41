# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Node

export var warn_at_scene_stack_size : int = 5

var scene_stack : Array = Array()
var pause_stack : Array = Array()

func _transition() -> void:
	var img : Image = get_viewport().get_texture().get_data()
	img.flip_y() 
	var trans : CanvasLayer = preload("res://Wish/WishTransition/Transitions/Multi/Transition_Multi.tscn").instance()
	trans.img = ImageTexture.new()
	trans.img.create_from_image(img)
	get_tree().get_root().call_deferred("add_child", trans)


func clear_stack() -> void:
	for scene in scene_stack:
		scene.queue_free()
	scene_stack.clear()
	pause_stack.clear()


func _change_scene(path : String) -> void:
	var err : int = get_tree().change_scene(path)
	if err:
		push_error("Could not change scene to %s (%d)" % [path, err])


func _remove_scene() -> Node:
	var scene : Node = get_tree().current_scene
	get_tree().current_scene = null
	get_tree().get_root().remove_child(scene)
	return scene


func _add_scene(scene : Node) -> void:
	assert(not get_tree().current_scene, "get_tree().current_scene not null")
	get_tree().get_root().add_child(scene)
	get_tree().current_scene = scene


func change_scene(path : String, pause : bool = false) -> void:
	clear_stack()
	_change_scene(path)
	_transition()
	get_tree().paused = pause


func push_scene(path : String, pause : bool = false) -> void:
	scene_stack.append(_remove_scene())
	pause_stack.append(get_tree().paused)
	if scene_stack.size() >= warn_at_scene_stack_size:
		push_warning("scene_stack.size() is %d" % scene_stack.size())
	_change_scene(path)
	_transition()
	get_tree().paused = pause


func pop_scene(pause_overwrite : bool = false, pause : bool = false) -> void:
	assert(scene_stack, "scene_stack is empty")
	_remove_scene().queue_free()
	_add_scene(scene_stack.pop_back())
	_transition()
	get_tree().paused = pause_stack.pop_back()
	if pause_overwrite:
		get_tree().paused = pause
	
