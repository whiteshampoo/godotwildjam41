# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+


extends CanvasLayer

export var tex : Texture = null
onready var viewport : Viewport = get_viewport()
onready var rect : Polygon2D = $Polygon

var img : Texture = null

signal finished

func _ready() -> void:
	_create_polygon()
	_update_polygon()
	viewport.connect("size_changed", self, "_update_polygon") # warning-ignore:return_value_discarded
	$AnimationPlayer.play("fade")
	if not tex:
		tex = NoiseTexture.new()
	if not tex.noise:
		tex.noise = OpenSimplexNoise.new()
	tex.width = int(rect.get_viewport_rect().size.x)
	tex.height = int(rect.get_viewport_rect().size.y)
	tex.noise.seed = randi()
	rect.material.set("shader_param/noise", tex)
	rect.texture = img


func _create_polygon() -> void:
	var points : PoolVector2Array = PoolVector2Array()
	for i in 4:
		points.append(Vector2.ZERO)
	rect.polygon = points
	_update_polygon()


func _update_polygon() -> void:
	rect.polygon[1].x = viewport.size.x
	rect.polygon[2].x = viewport.size.x
	rect.polygon[2].y = viewport.size.y
	rect.polygon[3].y = viewport.size.y


func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	emit_signal("finished")
	queue_free()
