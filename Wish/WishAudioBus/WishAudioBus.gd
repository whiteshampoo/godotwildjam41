# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Node
tool

const SAVE_MUTE : String = "audio %s mute"
const SAVE_VOLUME : String = "audio %s volume"

export var save_bus : PoolStringArray = PoolStringArray(["Master"])

func _ready() -> void:
	if Engine.editor_hint:
		return
	for bus in save_bus:
		if not bus_exists(bus):
			push_error("Bus %s does not exist" % bus)
			continue
		#WishSettings.init_value(SAVE_VOLUME % bus, get_volume(bus))
		#WishSettings.init_value(SAVE_MUTE % bus, get_mute(bus))
		WishSettings.init_value(SAVE_VOLUME % bus, 0.5)
		WishSettings.init_value(SAVE_MUTE % bus, false)
		set_volume(bus, WishSettings.get_value(SAVE_VOLUME % bus))
		set_mute(bus, WishSettings.get_value(SAVE_MUTE % bus))


func _get_bus(bus : String) -> int:
	var idx : int = AudioServer.get_bus_index(bus)
	if idx == -1:
		push_warning("Audio-Bus %s does not exist" % bus)
	return idx


func bus_exists(bus : String) -> bool:
	return AudioServer.get_bus_index(bus) >= 0


func set_mute(bus : String, mute : bool) -> void:
	var idx : int = _get_bus(bus)
	if idx == -1:
		return
	AudioServer.set_bus_mute(idx, mute)
	if not bus in save_bus:
		return
	var setting : String = SAVE_MUTE % bus
	if mute != WishSettings.get_value(setting):
		WishSettings.set_value(setting, mute)


func get_mute(bus : String) -> bool:
	var idx : int = _get_bus(bus)
	if idx == -1:
		return false
	return AudioServer.is_bus_mute(idx)


func set_volume(bus : String, linear : float, clamped : bool = true) -> void:
	var idx : int = _get_bus(bus)
	if idx == -1:
		return
	if clamped:
		linear = clamp(linear, 0.0, 1.0)
	AudioServer.set_bus_volume_db(idx, linear2db(linear))
	if not bus in save_bus:
		return
	var setting : String = SAVE_VOLUME % bus
	if not is_equal_approx(linear, WishSettings.get_value(setting)):
		WishSettings.set_value(setting, linear)


func get_volume(bus : String, clamped : bool = true) -> float:
	var idx : int = _get_bus(bus)
	if idx == -1:
		return 0.0
	var linear : float = db2linear(AudioServer.get_bus_volume_db(idx))
	if clamped:
		linear = clamp(linear, 0.0, 1.0)
	return linear
