# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Node

export var _filename : String = "settings.txt"
export var _filename_rescue : String = "settings_rescue.txt"

var _settings : Dictionary = Dictionary()
var _settings_rescue : Dictionary = Dictionary()

func _ready() -> void:
	_filename_rescue = "res://" + _filename_rescue
	_settings_rescue = load_settings(_filename_rescue)
	
	if OS.has_feature("editor"):
		_filename = "res://" + _filename
	else:
		_filename = "user://" + _filename
	#print("Load settings from %s" % _filename)
	
	if not File.new().file_exists(_filename):
		print("Create new settings-file")
		save_settings(_filename, _settings)
	_settings = load_settings(_filename)
	
		

func set_value(key : String, value) -> void:
	if not key in _settings:
		_settings[key] = value
		save_settings(_filename, _settings)
		return
		
	if _settings[key] != value:
		_settings[key] = value
		save_settings(_filename, _settings)


func get_value(key : String):
	if not key in _settings:
		push_warning("Settings-Key '%s' not found" % key)
		#assert(key in _settings_rescue, "No rescue-key found")
		set_value(key, _settings_rescue[key])
	return _settings[key]


func init_value(key : String, value) -> void:
	if OS.has_feature("editor"):
		_settings_rescue[key] = value
		save_settings(_filename_rescue, _settings_rescue)
	_settings_rescue[key] = value
	if key in _settings:
		return
	_settings[key] = value
	save_settings(_filename, _settings)


func load_settings(_file : String) -> Dictionary:
	if not _file:
		push_error("No filename")
		return Dictionary()
	var file : File = File.new()
	var err : int = file.open(_file, File.READ)
	if err == 7:
		push_warning("Settings-file not found")
		return Dictionary()# file not found
	if err:
		push_error("Could not open settingsfile. Error %d" % err)
		return Dictionary()
	var data : String = file.get_as_text()
	file.close()
	return str2var(data)
	

func save_settings(_file : String, _set : Dictionary) -> void:
	if not _file:
		push_error("No filename")
		return
	var file : File = File.new()
	var err : int = file.open(_file, File.WRITE)
	if err:
		push_error("Could not open settingsfile. Error %d" % err)
		return
	var data : String = var2str(_set)
	file.store_string(data)
	file.close()


func factory_reset() -> void:
	_settings = _settings_rescue
	save_settings(_filename, _settings)
