# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Node

export (Array, Translation) var translations : Array

var languages : Dictionary = Dictionary()

func _ready() -> void:
	WishSettings.init_value("language", "en")
	for translation in translations:
		TranslationServer.add_translation(translation)
		languages[translation.locale] = translation.get_message("§_LANG")
	update_translation()


func set_translation(language : String) -> void:
	print("set locale ", language)
	TranslationServer.set_locale(language)

func update_translation() -> void:
	set_translation(WishSettings.get_value("language"))
