# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends HBoxContainer
class_name WishBusControl
#tool
#
#export var bus : String = "Master"
#export var label : String = "" setget set_label
#
#onready var Name : Label = $Name
#onready var Volume : HSlider = $Volume
#onready var Mute : Button = $Mute
#
#func set_label(l : String) -> void:
#	label = l
#	if not Name:
#		return
#	if label:
#		Name.text = label
#		return
#	if bus:
#		Name.text = bus
#		return
#	Name.text = "N/A"
#
#func _ready() -> void:
#	set_label(label)
#	if Engine.editor_hint:
#		return
#
#	assert(bus, "Bus cannot be an empty string.")
#	assert(Wish.AudioBus.bus_exists(bus), "Bus %s does not exist." % bus)
#	Mute.pressed = Wish.AudioBus.get_mute(bus) # triggers signal and disables slider
#	Volume.value = Wish.AudioBus.get_volume(bus)
#
#
#func _on_Volume_value_changed(value: float) -> void:
#	Wish.AudioBus.set_volume(bus, value)
#
#
#func _on_Mute_toggled(button_pressed: bool) -> void:
#	Wish.AudioBus.set_mute(bus, button_pressed)
#	Volume.editable = not button_pressed
