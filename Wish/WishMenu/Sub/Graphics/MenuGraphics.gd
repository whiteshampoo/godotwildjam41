# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Control

onready var Gamma : WishModSlider = $Container/Left/Gamma
onready var Brightness : WishModSlider = $Container/Left/Brightness
onready var Contrast : WishModSlider = $Container/Left/Contrast
onready var Saturation : WishModSlider = $Container/Left/Saturation

func _ready() -> void:
#	Gamma.mod_value = WishSettings.get_value("gamma")
#	Brightness.mod_value = WishSettings.get_value("brightness")
#	Contrast.mod_value = WishSettings.get_value("contrast")
#	Saturation.mod_value = WishSettings.get_value("saturation")
	Gamma.mod_value = WishCA.gamma
	Brightness.mod_value = WishCA.brightness
	Contrast.mod_value = WishCA.contrast
	Saturation.mod_value = WishCA.saturation


func _on_Brightness_value_changed(value : float) -> void:
	WishCA.brightness = value


func _on_Contrast_value_changed(value : float) -> void:
	WishCA.contrast = value


func _on_Saturation_value_changed(value : float) -> void:
	WishCA.saturation = value


func _on_Gamma_value_changed(value : float) -> void:
	WishCA.gamma = value
