# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends VBoxContainer


func _ready() -> void:
	$Menus.current_tab = 1 # Not BACK


func goto_last_menu() -> void:
	WishTransition.pop_scene()
	WishTranslation.call_deferred("update_translation")


func _on_Menus_tab_selected(tab : int) -> void:
	if tab == 0:
		goto_last_menu()
