# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Control
