# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends HBoxContainer
tool

onready var Name : Label = $Name
onready var Slide : HSlider = $Slide
onready var Mute : Button = $Mute

export var name_label : String = "SliderLabel" setget set_name_label
func set_name_label(new_name) -> void:
	name_label = new_name
	if is_instance_valid(Name):
		Name.text = name_label
	
	
export var name_bus : String = "Busname" setget set_name_bus
func set_name_bus(new_name) -> void:
	name_bus = new_name
	update_configuration_warning()


func _get_configuration_warning() -> String:
	if not WishAudioBus.bus_exists(name_bus):
		return "Bus '%s' not found" % name_bus
	return ""


func _ready() -> void:
	if Engine.editor_hint:
		return
	if not WishAudioBus.bus_exists(name_bus):
		push_error("Bus '%s' not found" % name_bus)
	Name.text = name_label
	Slide.value = WishAudioBus.get_volume(name_bus)
	Mute.pressed = WishAudioBus.get_mute(name_bus)
	

func _on_Slide_value_changed(value: float) -> void:
	if is_equal_approx(value, WishAudioBus.get_volume(name_bus)):
		return
	WishAudioBus.set_volume(name_bus, value)


func _on_Default_toggled(button_pressed: bool) -> void:
	Slide.editable = not button_pressed
	WishAudioBus.set_mute(name_bus, button_pressed)
