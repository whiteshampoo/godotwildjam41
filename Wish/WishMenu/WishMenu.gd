# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Control
class_name WishMenu

signal start
signal settings
signal exit


func _on_Settings_pressed() -> void:
	emit_signal("settings")


func _on_Exit_pressed() -> void:
	emit_signal("exit")


func _on_Start_pressed() -> void:
	emit_signal("start")
