# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends CanvasLayer

export (String, FILE, "*.tscn") var main_menu : String = "res://Wish/WishMenu/WishMenu.tscn"
export (String, FILE, "*.tscn") var settings : String = "res://Wish/WishMenu/Sub/SubMenu.tscn"

onready var BG : ColorRect = $BG
onready var Fullscreen : Button = $BG/Center/Container/Fullscreen
var original_layer : int = layer

var done : bool = false


func init(tree : SceneTree) -> void:
	# this is bullshit :D
	assert(not get_parent(), "Dont add the Ingame-Menu as a child. Just init(get_tree())")
	if get_parent():
		get_parent().remove_child(self)
	done = true
	tree.get_root().add_child(self)


func _ready() -> void:
	#assert(done, "No init(get_tree())!")
	#if not done:
	#	init(get_tree())
	get_tree().paused = true
	_on_Fullscreen_toggled(OS.window_fullscreen)
# warning-ignore:return_value_discarded
	FullscreenHandler.connect("toggle_fullscreen", self, "_toggle_fullscreen")


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_on_Continue_pressed()
	layer = original_layer


func _update_nodetree() -> void:
	if not self.is_inside_tree() or not get_tree().current_scene:
		return
	layer = 1
	var own_position : int = get_position_in_parent()
	var main_position : int = get_tree().current_scene.get_position_in_parent()
	if own_position < main_position:
		get_tree().get_root().move_child(self, main_position)

func _connect_submenu(signal_name : String) -> void:
# warning-ignore:return_value_discarded
	get_tree().current_scene.connect(signal_name, self, "_update_nodetree", [], CONNECT_DEFERRED)

func _on_Continue_pressed() -> void:
	get_tree().paused = false
	queue_free()


func _on_Settings_pressed() -> void:
	layer = -1
	WishTransition.push_scene(settings)
	#call_deferred("_connect_submenu", "tree_exited")


func _on_MainMenu_pressed() -> void:
	WishTransition.change_scene(main_menu)
	queue_free()


func _on_Fullscreen_toggled(button_pressed : bool) -> void:
	OS.window_fullscreen = button_pressed
	Fullscreen.set_deferred("pressed", button_pressed)
	if button_pressed:
		Fullscreen.icon = preload("res://UI/Icons/smaller.png")
		WishSettings.set_value("fullscreen", true)
	else:
		Fullscreen.icon = preload("res://UI/Icons/larger.png")
		WishSettings.set_value("fullscreen", false)


func _toggle_fullscreen() -> void:
	call_deferred("_on_Fullscreen_toggled", OS.window_fullscreen)
