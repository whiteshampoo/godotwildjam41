# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |                     License: CC0                     |
# |  https://creativecommons.org/publicdomain/zero/1.0/  |
# |                                                      |
# +------------------------------------------------------+

extends Polygon2D

const SAVE_BRIGHTNESS : String = "color brightness"
const SAVE_CONTRAST : String = "color contrast"
const SAVE_SATURATION : String = "color saturation"
const SAVE_GAMMA : String = "color gamma"

onready var viewport : Viewport = get_viewport()

export(float, 0.1, 2.0) var brightness : float = 1.0 setget set_brightness
export(float, 0.1, 2.0) var contrast : float = 1.0 setget set_contrast
export(float, 0.0, 2.0) var saturation : float = 1.0 setget set_saturation
export(float, 0.1, 3.0) var gamma : float = 1.0 setget set_gamma


func set_brightness(value : float) -> void:
	brightness = value
	WishSettings.set_value(SAVE_BRIGHTNESS, value)
	material.set_shader_param("brightness", brightness)


func set_contrast(value : float) -> void:
	contrast = value
	WishSettings.set_value(SAVE_CONTRAST, value)
	material.set_shader_param("contrast", contrast)


func set_saturation(value : float) -> void:
	saturation = value
	WishSettings.set_value(SAVE_SATURATION, value)
	material.set_shader_param("saturation", saturation)


func set_gamma(value : float) -> void:
	gamma = value
	WishSettings.set_value(SAVE_GAMMA, value)
	material.set_shader_param("gamma", gamma)


func _ready() -> void:
	WishSettings.init_value(SAVE_BRIGHTNESS, 1.0)
	WishSettings.init_value(SAVE_CONTRAST, 1.0)
	WishSettings.init_value(SAVE_SATURATION, 1.0)
	WishSettings.init_value(SAVE_GAMMA, 1.0)
	
	brightness = WishSettings.get_value(SAVE_BRIGHTNESS)
	contrast = WishSettings.get_value(SAVE_CONTRAST)
	saturation = WishSettings.get_value(SAVE_SATURATION)
	gamma = WishSettings.get_value(SAVE_GAMMA)
	
	viewport.connect("size_changed", self, "_update_polygon") # warning-ignore:return_value_discarded
	_create_polygon()
	_update_all()


func _create_polygon() -> void:
	var points : PoolVector2Array = PoolVector2Array()
	for i in 4:
		points.append(Vector2.ZERO)
	polygon = points
	_update_polygon()


func _update_polygon() -> void:
	polygon[1].x = viewport.size.x
	polygon[2].x = viewport.size.x
	polygon[2].y = viewport.size.y
	polygon[3].y = viewport.size.y


func _update_all() -> void:
	set_brightness(brightness)
	set_contrast(contrast)
	set_saturation(saturation)
	set_gamma(gamma)
