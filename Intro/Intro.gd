# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Control

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export (String, FILE) var skip_to_scene : String = ""
export (String, FILE) var menu_scene : String = ""
export (String, FILE) var lang_scene : String = ""

#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------



#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	WishSettings.init_value("fullscreen", false)
	if OS.get_name() == "HTML5":
		WishSettings.set_value("fullscreen", false)
	OS.window_fullscreen = WishSettings.get_value("fullscreen")
	randomize()
	assert(menu_scene, "No menu scene defined")
	assert(lang_scene, "No language scene defined")
	if skip_to_scene and not OS.has_feature("standalone"):
		#var err : int = 
		WishTransition.change_scene(skip_to_scene)
		#assert(not err, "Cannot load %s (Error: %d)" % [skip_to_scene, err])
		return
	$IntroPlayer.play("Intro")


func _input(event: InputEvent) -> void:
	if event.is_action("ui_cancel"):
		leave_intro()


#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func leave_intro() -> void:
	MusicPlayer.play_random_track()
	if not WishSettings.get_value("lang_init"):
		WishTransition.change_scene(lang_scene)
		return
	WishTransition.change_scene(menu_scene)

#-[SIGNAL METHODS]--------------------------------------------------------------
