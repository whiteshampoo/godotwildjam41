# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Control

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export (String, FILE, "*.tscn") var campaign : String = ""
export (String, FILE, "*.tscn") var random : String = ""
export (String, FILE, "*.tscn") var settings : String = ""
export (String, FILE, "*.tscn") var credits : String = ""

#-[ONREADY]---------------------------------------------------------------------

onready var Fullscreen : Button = $Container/Buttons/Fullscreen

#-[VAR]-------------------------------------------------------------------------



#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(campaign, "No campaing-scene")
	assert(random, "No random-battle-scene")
	assert(settings, "No settings-scene")
	assert(credits, "No credits-scene")
	if OS.get_name() == "HTML5":
		$Container/Buttons/Exit.visible = false
	
	_on_Fullscreen_toggled(OS.window_fullscreen)
# warning-ignore:return_value_discarded
	FullscreenHandler.connect("toggle_fullscreen", self, "_toggle_fullscreen")

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------



#-[SIGNAL METHODS]--------------------------------------------------------------

func _on_Campaign_pressed() -> void:
	WishTransition.change_scene(campaign)


func _on_Random_pressed() -> void:
	WishTransition.change_scene(random)


func _on_Load_pressed() -> void:
	print("not implemented yet")


func _on_Settings_pressed() -> void:
	WishTransition.push_scene(settings)


func _on_Credits_pressed() -> void:
	WishTransition.push_scene(credits)


func _on_Exit_pressed() -> void:
	get_tree().quit()



func _on_Fullscreen_toggled(button_pressed : bool) -> void:
	OS.window_fullscreen = button_pressed
	Fullscreen.set_deferred("pressed", button_pressed)
	if button_pressed:
		Fullscreen.icon = preload("res://UI/Icons/smaller.png")
		WishSettings.set_value("fullscreen", true)
	else:
		Fullscreen.icon = preload("res://UI/Icons/larger.png")
		WishSettings.set_value("fullscreen", false)


func _toggle_fullscreen() -> void:
	call_deferred("_on_Fullscreen_toggled", OS.window_fullscreen)
