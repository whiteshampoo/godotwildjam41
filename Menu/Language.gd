# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Control

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------



#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------



#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	WishSettings.init_value("lang_init", false)

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------



#-[SIGNAL METHODS]--------------------------------------------------------------

func _on_English_pressed() -> void:
	WishTranslation.set_translation("en")
	WishSettings.set_value("language", "en")
	WishSettings.set_value("lang_init", true)
	WishTransition.change_scene("res://Menu/Menu.tscn")


func _on_Deutsch_pressed() -> void:
	WishTranslation.set_translation("de")
	WishSettings.set_value("language", "de")
	WishSettings.set_value("lang_init", true)
	WishTransition.change_scene("res://Menu/Menu.tscn")
