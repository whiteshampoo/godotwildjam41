# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends "res://Objects/BuildMenu/BuildMenuTemplate.gd"

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------



#-[ONREADY]---------------------------------------------------------------------

onready var cost : int = map.get_new_tower_cost()
onready var button_tower : Button = $ButtonContainer/Tower

#-[VAR]-------------------------------------------------------------------------



#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	button_tower.set_cost(cost)
	test_gold(Overlay.gold)

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func test_gold(gold : int) -> void:
	button_tower.disabled = gold < button_tower.cost

#-[SIGNAL METHODS]--------------------------------------------------------------

func _gold_changed(new_gold : int) -> void:
	test_gold(new_gold)
