# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends "res://Objects/BuildMenu/BuildMenuTemplate.gd"

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------



#-[ONREADY]---------------------------------------------------------------------

onready var Cannon : Button = $ButtonContainer/Cannon
onready var Archer : Button = $ButtonContainer/Archer
onready var Booster : Button = $ButtonContainer/Booster
onready var UpgradeSpeed : Button = $ButtonContainer/UpgradeSpeed
onready var UpgradeRange : Button = $ButtonContainer/UpgradeRange
onready var Sell : Button = $ButtonContainer/Sell

#-[VAR]-------------------------------------------------------------------------



#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	turret = map.get_tower(target)
	assert(turret, "no turret found")
	Cannon.cost *= turret.turrets.size() + 1
	Archer.cost *= turret.turrets.size() + 1
	Booster.cost *= turret.boosters * 2 + 1
	Sell.cost = -int((turret.money_spent + map.get_new_tower_cost(-1)) / 2.0)
	if not Sell.cost:
		Sell.text = "§_DESTROY"
	match turret.tier:
		0:
			UpgradeSpeed.cost = 100
			UpgradeRange.cost = 100
		1:
			UpgradeSpeed.cost = 500
			UpgradeRange.cost = 500
		_: 
			UpgradeSpeed.cost = 0
			UpgradeRange.cost = 0
			UpgradeSpeed.visible = false
			UpgradeRange.visible = false
	test_gold(Overlay.gold)
	assert(turret.turrets.size() <= 4, "Too many turrets")
	if turret.turrets.size() == 4:
		Cannon.visible = false
		Archer.visible = false
		Booster.visible = false
		


func _process(_delta : float) -> void:
	pass


func _physics_process(_delta : float) -> void:
	pass



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func test_gold(gold : int) -> void:
	Cannon.disabled = gold < Cannon.cost
	Archer.disabled = gold < Archer.cost
	Booster.disabled = gold < Booster.cost
	UpgradeSpeed.disabled = gold < UpgradeSpeed.cost or turret.tier == 2
	UpgradeRange.disabled = gold < UpgradeRange.cost or turret.tier == 2

#-[SIGNAL METHODS]--------------------------------------------------------------

func _gold_changed(new_gold : int) -> void:
	test_gold(new_gold)
