# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Panel

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------

signal button_pressed(button)

#-[EXPORT]----------------------------------------------------------------------



#-[ONREADY]---------------------------------------------------------------------

onready var Overlay : CanvasLayer = get_tree().get_nodes_in_group("Overlay")[0]

#-[VAR]-------------------------------------------------------------------------

var map : TileMap = null
var target : Vector2 = Vector2.ZERO
var initialized : bool = false
var turret : Area2D = null
var have_mouse : bool = false

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(initialized, "init was not called")
	for node in $ButtonContainer.get_children():
		if node.is_in_group("BuildButton"):
			node.connect("button_pressed", self, "_button_pressed")
# warning-ignore:return_value_discarded
	Overlay.connect("gold_changed", self, "_gold_changed")

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func init(_map : TileMap, _target : Vector2) -> void:
	initialized = true
	map = _map
	target = _target


func add_sound(node : AudioStreamPlayer) -> void:
	remove_child(node)
	get_parent().add_child(node)
# warning-ignore:return_value_discarded
	node.connect("finished", node, "queue_free")
	node.pitch_scale = rand_range(0.9, 1.1)
	node.play()

#-[SIGNAL METHODS]--------------------------------------------------------------

func _button_pressed(button_id : String, minus_gold : int) -> void:
	emit_signal("button_pressed", button_id)
	match button_id:
		"tower":
			turret = map.build(target)
			turret.money_spent = -minus_gold
			add_sound($Tower)
		"cannon":
			assert(turret, "no turret")
			turret.add_turret(preload("res://Objects/Units/Turret/Cannon.tscn"))
			add_sound($Turret)
		"archer":
			assert(turret, "no turret")
			turret.add_turret(preload("res://Objects/Units/Turret/Archer.tscn"))
			add_sound($Turret)
		"booster":
			assert(turret, "no turret")
			turret.add_turret(preload("res://Objects/Units/Turret/Booster.tscn"))
			turret.boost()
			add_sound($Turret)
		"upgrade_speed":
			assert(turret, "no turret")
			turret.upgrade(true, false)
			add_sound($Upgrade)
		"upgrade_range":
			assert(turret, "no turret")
			turret.upgrade(false, true)
			add_sound($Upgrade)
		"sell":
			assert(turret, "no turret")
			turret.queue_free()
			map.unbuild(turret.global_position)
			add_sound($Sell)
		_:
			assert(false, "Unknown button_id '%s'" % button_id)
	turret.money_spent += minus_gold
	Overlay.mod_gold(-minus_gold)
	queue_free()

# warning-ignore:unused_argument
func _gold_changed(new_gold : int) -> void:
	push_warning("_gold_changed not overwritten")


func _on_BuildMenu_mouse_entered() -> void:
	have_mouse = true


func _on_BuildMenu_mouse_exited() -> void:
	have_mouse = false
