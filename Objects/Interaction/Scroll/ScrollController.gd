# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Node2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export (float, 32.0, 1024.0, 8.0) var scroll_speed : float = 256.0


#-[ONREADY]---------------------------------------------------------------------

onready var cam : Camera2D = get_parent()
onready var mouse_rect : Vector2 = $Shape.shape.extents

#-[VAR]-------------------------------------------------------------------------

var can_scroll : bool = true

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(cam is Camera2D, "Parent ist not a Camera2D")


func _process(delta : float) -> void:
	if not can_scroll: return
	var abs_mouse : Vector2 = get_local_mouse_position().abs()
	if abs_mouse.x < mouse_rect.x: abs_mouse.x = 0
	if abs_mouse.y < mouse_rect.y: abs_mouse.y = 0
	if not abs_mouse: return
	
	var x : float = range_lerp(abs_mouse.x, mouse_rect.x, get_viewport_rect().size.x / 2.0, 0.0, 1.0)
	var y : float = range_lerp(abs_mouse.y, mouse_rect.y, get_viewport_rect().size.y / 2.0, 0.0, 1.0)
	cam.position += get_local_mouse_position().normalized() * max(x, y) * scroll_speed * delta
	cam.position.x = clamp(cam.position.x, cam.limit_left + 1280 / 2, cam.limit_right - 1280 / 2)
	cam.position.y = clamp(cam.position.y, cam.limit_top + 720 / 2, cam.limit_bottom - 720 / 2)

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------



#-[SIGNAL METHODS]--------------------------------------------------------------
