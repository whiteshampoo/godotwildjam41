# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Node2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var map_buildable : NodePath = ""
export var build_menu_tower : PackedScene = preload("res://Objects/BuildMenu/BuildMenuTower.tscn")
export var build_menu_upgrade : PackedScene = preload("res://Objects/BuildMenu/BuildMenuUpgrade.tscn")

#-[ONREADY]---------------------------------------------------------------------

onready var MapBuildable : TileMap = get_node(map_buildable)
onready var Cursor : Sprite = $Cursor
onready var Cam : Camera2D = get_node("../Camera2D")

#-[VAR]-------------------------------------------------------------------------

var build_menu : Panel = null
var can_build = true

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(MapBuildable, "Cannot found buildmap (%s)" % map_buildable)
	assert(MapBuildable.is_in_group("MapBuildable"), "Wrong map for buildmap (%s)" % map_buildable)


func _process(_delta : float) -> void:
	if not can_build: return
	if is_instance_valid(build_menu):
		if Input.is_action_just_pressed("mouse_right"):
			build_menu.queue_free()
			build_menu = null
			get_parent().scrolling(true)
		elif Input.is_action_just_pressed("mouse_left"):
			if not build_menu.have_mouse:
				build_menu.queue_free()
				build_menu = null
				get_parent().scrolling(true)
		return
			
	var mouse : Vector2 = get_global_mouse_position()
	var content : int = MapBuildable.get_cell_content(mouse)
	match content:
		MapBuildable.BLOCKED: 
			Cursor.visible = false
		MapBuildable.FREE, MapBuildable.TOWER:
			Cursor.visible = true
			Cursor.modulate = Color.white if content == MapBuildable.FREE else Color.yellow
			Cursor.global_position = MapBuildable.get_global_cell_position(mouse)
			if Input.is_action_just_pressed("mouse_left"):
				if is_instance_valid(build_menu): return
				match content:
					MapBuildable.FREE:
						build_menu = build_menu_tower.instance()
					MapBuildable.TOWER:
						build_menu = build_menu_upgrade.instance()
				build_menu.init(MapBuildable, Cursor.global_position)
				Cam.add_child(build_menu)
				build_menu.rect_position = -build_menu.rect_size / 2 
				if Cam.get_local_mouse_position().x < 0.0:
					build_menu.rect_position.x += 1280 / 4
				else:
					build_menu.rect_position.x -= 1280 / 4
# warning-ignore:return_value_discarded
				build_menu.connect("button_pressed", self, "_build_button_pressed")
				get_parent().scrolling(false)
		_:
			assert(false, "unknown content (%d)" % content)



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------



#-[SIGNAL METHODS]--------------------------------------------------------------

func _build_button_pressed(_button_id : String) -> void:
		build_menu = null
		get_parent().scrolling(true)

