# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends CanvasLayer

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------

signal gold_changed(new_gold)
signal lives_changed(new_lives)

#-[EXPORT]----------------------------------------------------------------------

export (int, 0, 1000, 10) var gold : int = 1000
export (int, 1, 10, 1) var lives : int = 5

#-[ONREADY]---------------------------------------------------------------------

onready var Credits : Label = $Credits
onready var Lives : TextureProgress = $Lives
onready var FishButton : TextureButton = $FishButton
onready var ScrollController : Node2D = get_node("../Camera2D/ScrollController")
onready var CursorController : Node2D = get_node("../CursorController")

#-[VAR]-------------------------------------------------------------------------

var is_fishing : bool = false
var fishing : ColorRect = null

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	set_gold(gold)
	set_lives(lives)

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func set_gold(value : int) -> void:
	var old_gold = gold
	gold = value
	Credits.text = tr("§_GOLD") + ": " + str(gold)
	if gold != old_gold:
		emit_signal("gold_changed", gold)


func mod_gold(value : int) -> void:
	var new_gold : int = gold + value
	if new_gold < 0:
		push_warning("Gold less zero")
		new_gold = 0
	set_gold(new_gold)


func set_lives(value : int) -> void:
	var old_lives : int = value
	lives = value
	Lives.value = lives
	if lives != old_lives:
		emit_signal("lives_changed", lives)


func mod_lives(value : int) -> void:
	var new_lives : int = lives + value
	if new_lives < 0:
		new_lives = 0
	set_lives(new_lives)


func kill_fish() -> void:
	if not is_instance_valid(fishing): return
	fishing.queue_free()
	fishing = null
	_finished_fishing()
	

#-[SIGNAL METHODS]--------------------------------------------------------------


func _on_FishButton_pressed() -> void:
	if is_instance_valid(fishing): return
	CursorController.can_build = false
	ScrollController.can_scroll = false
	is_fishing = true
	fishing = preload("res://Objects/Fishing/FishContainer.tscn").instance()
	fishing.connect("finished", self, "_finished_fishing") # warning-ignore:return_value_discarded
	fishing.connect("money", self, "_fishing_money") # warning-ignore:return_value_discarded
	add_child(fishing)


func _finished_fishing() -> void:
	CursorController.can_build = true
	ScrollController.can_scroll = true
	is_fishing = false
	fishing = null


func _fishing_money(value : int) -> void:
	mod_gold(value)
