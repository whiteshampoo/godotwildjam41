# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Area2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var gradient : Gradient = Gradient.new()

#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------

var failing : float = 0.0
var can_fish : bool = false
var fishing : bool = false

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _process(_delta : float) -> void:
	update()


func _draw() -> void:
	if can_fish:
		draw_circle(Vector2.ZERO, 16.0, Color(1.0, 1.0, 0.0, 0.5))
	if fishing:
		var color : Color = gradient.interpolate(failing)
		draw_circle(Vector2.ZERO, $Size.shape.radius, color)

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------



#-[SIGNAL METHODS]--------------------------------------------------------------
