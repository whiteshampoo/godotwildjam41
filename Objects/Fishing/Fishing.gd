# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Node2D

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------

signal money(value)

#-[EXPORT]----------------------------------------------------------------------

export var gradient : Gradient

#-[ONREADY]---------------------------------------------------------------------

onready var Rod : Sprite = $Rod
onready var Crank : Sprite = $Mechanic/Crank
onready var Anim : AnimationPlayer = $AnimationPlayer
onready var MouseCatcher : Area2D = $Mechanic/Crank/MouseCatcher
onready var T : Tween = $Tween
onready var Finish : Timer = $Finish

onready var Layer1 : TileMap = $WaterLayer1
onready var Layer2 : TileMap = $WaterLayer2
onready var Layer3 : TileMap = $WaterLayer3
onready var Layer4 : TileMap = $WaterLayer4


#-[VAR]-------------------------------------------------------------------------

var idle : bool = true
var mouse_catched = false
var failing : float = 0.0

var time : float = 0.0

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	MouseCatcher.failing = failing
	MouseCatcher.can_fish = true


func _process(delta: float) -> void:
	time += delta
	
	Layer1.position.x = fmod(Layer1.position.x + delta * 16.0 + sin(time) * delta * 8.0, 64.0)
	Layer2.position.x = fmod(Layer2.position.x + delta * 14.0 + sin(time + PI) * delta * 10.0, 64.0)
	Layer3.position.x = fmod(Layer3.position.x + delta * 13.0 + sin(time - PI / 2.0) * delta * 4.0, 64.0)
	Layer4.position.x = fmod(Layer4.position.x + delta * 20.0 + sin(time + PI / 3.0) * delta * 6.0, 64.0)
	
	Layer1.position.y += sin(time - PI / 3.0) * delta * 4.0
	Layer2.position.y += sin(time) * delta * 4.0
	Layer3.position.y += sin(time + PI / 2.0) * delta * 4.0
	Layer4.position.y += sin(time + PI / 5.0) * delta * 4.0


func _physics_process(delta : float) -> void:
	if not idle and not mouse_catched:
		failing = min(failing + delta / 2.0, 1.0)
		MouseCatcher.failing = failing

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func make_tween() -> void:
	var time_left : float = Anim.current_animation_length - Anim.current_animation_position
	var time_rnd : float = min(time_left, min(1.0 + randf() * time_left, 3.0))
	if time_left <= 1.0:
		time_rnd = 1.0
	var max_rotation : float = time_rnd * TAU / 4.0
	var rnd_rotation : float = 0.1 + (randf() * 0.9) * max_rotation
# warning-ignore:return_value_discarded
	T.interpolate_property(Crank, "rotation", null, Crank.rotation + rnd_rotation, time_rnd, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	T.interpolate_method(Rod, "tween", 0.0, 1.0, time_rnd, Tween.TRANS_LINEAR)
	Rod.new_tween()
# warning-ignore:return_value_discarded
	T.start()

#-[SIGNAL METHODS]--------------------------------------------------------------


func _on_MouseCatcher_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if not idle: return
	if not event is InputEventMouseButton: return
	if not event.pressed or not event.button_index == BUTTON_LEFT: return
	idle = false
	Anim.play("fish")
	make_tween()
	failing = 0.0
	MouseCatcher.failing = 0.0
	MouseCatcher.can_fish = false
	MouseCatcher.fishing = true
		


func _on_MouseCatcher_mouse_entered() -> void:
	mouse_catched = true


func _on_MouseCatcher_mouse_exited() -> void:
	mouse_catched = false


func _on_Tween_tween_all_completed() -> void:
	if not idle:
		make_tween()
	else:
		MouseCatcher.can_fish = true

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "RESET": return
	idle = true
	Anim.play("RESET")
	MouseCatcher.fishing = false
	Rod.finish_tween(failing < 0.9)
	if failing < 0.9:
		emit_signal("money", int(100.0 * (1.0 - failing)))
# warning-ignore:return_value_discarded
	T.interpolate_method(Rod, "tween", 0.0, 1.0, 1.0, Tween.TRANS_LINEAR)
# warning-ignore:return_value_discarded
	T.start()
	Finish.start()


func _on_Finish_timeout() -> void:
	Rod.start_tween()
# warning-ignore:return_value_discarded
	T.interpolate_method(Rod, "tween", 0.0, 1.0, 0.5, Tween.TRANS_LINEAR)
# warning-ignore:return_value_discarded
	T.start()

