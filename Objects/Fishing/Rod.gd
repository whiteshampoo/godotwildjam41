# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Sprite

#-[CONSTANTS]---------------------------------------------------------------------



#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var dead_fish : PackedScene = preload("Fish/DeadFish.tscn")
export var living_fish : PackedScene = preload("Fish/LivingFish.tscn")

#-[ONREADY]---------------------------------------------------------------------

onready var HookShape : CollisionShape2D = $Line/HookShape
onready var EndLine : Position2D = $Line/EndLine
onready var Line : Line2D = $Line
onready var Hook : Line2D = $Line/Hook

#-[VAR]-------------------------------------------------------------------------

var pos_start : Vector2 = Vector2.ZERO
var pos_end : Vector2 = Vector2.ZERO
var fish : Sprite = null

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func new_tween() -> void:
	pos_start = Line.points[1]
	pos_end.x = HookShape.position.x + (randf() * 2.0 - 1.0) * HookShape.shape.extents.x
	pos_end.y = HookShape.position.y + (randf() * 2.0 - 1.0) * HookShape.shape.extents.y


func finish_tween(success : bool) -> void:
	pos_start = Line.points[1]
	pos_end = EndLine.position
	if success:
		fish = living_fish.instance()
	else:
		fish = dead_fish.instance()
	Line.add_child(fish)


func start_tween() -> void:
	pos_start = Line.points[1]
	pos_end = HookShape.position
	if is_instance_valid(fish):
		fish.queue_free()
	fish = null


func tween(progress : float) -> void:
	Line.points[1] = lerp(pos_start, pos_end, progress)
	if is_instance_valid(fish):
		fish.position = Line.points[1]
	Hook.position = Line.points[1]

#-[SIGNAL METHODS]--------------------------------------------------------------
