# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends TileMap

#-[CONSTANTS]---------------------------------------------------------------------

const BLOCKED = -1
const FREE = 0
const TOWER = 1

#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------

export var tower_scene : PackedScene = preload("res://Objects/Units/Tower/TowerTemplate.tscn")

#-[ONREADY]---------------------------------------------------------------------



#-[VAR]-------------------------------------------------------------------------

var sand_map : TileMap = null
var tower_dict : Dictionary = Dictionary()

#-[SETGET METHODS]--------------------------------------------------------------



#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	clear()
	sand_map = get_tree().get_nodes_in_group("MapSand")[0]
	var decoration : Array = get_tree().get_nodes_in_group("MapDecoration")

	for cell in sand_map.buildable.keys():
		set_cellv(cell, FREE)
	
	for layer in decoration:
		for cell in layer.get_used_cells():
			set_cellv(cell, BLOCKED)

#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------


func global_to_map(global_coords : Vector2) -> Vector2:
	return world_to_map(to_local(global_coords))


func get_cell_content(global_coords : Vector2) -> int:
	match get_cellv(world_to_map(to_local(global_coords))):
		-1: return BLOCKED
		0: return FREE
		_: return TOWER


func build(global_coords : Vector2) -> Node2D:
	var cell : Vector2 = world_to_map(to_local(global_coords))
	assert(get_cellv(cell) == FREE, "cannot build on %s (%d)" % [cell, get_cellv(cell)])
	set_cellv(cell, TOWER)
	sand_map.block_tile(cell)
	var tower : Area2D = tower_scene.instance()
	tower_dict[cell] = tower
	tower.position = get_global_cell_position(global_coords)
	add_child(tower)
	return tower


func unbuild(global_coords : Vector2) -> void:
	var cell : Vector2 = world_to_map(to_local(global_coords))
	assert(get_cellv(cell) == TOWER, "cannot unbuild %s (%d)" % [cell, get_cellv(cell)])
	set_cellv(cell, FREE)
	sand_map.unblock_tile(cell)
# warning-ignore:return_value_discarded
	tower_dict.erase(global_to_map(global_coords))


func get_global_cell_position(global_coords : Vector2) -> Vector2:
	return global_position + global_to_map(global_coords) * 32.0 + Vector2.ONE * 16.0


func get_tower(global_coords : Vector2) -> Node2D:
	assert(world_to_map(global_coords) in tower_dict, "No tower found")
	return tower_dict[world_to_map(global_coords)]


func get_new_tower_cost(offset : float = 0) -> int:
	var x : float = tower_dict.size() + offset
	return int(x * 10.0 + floor(x / 10.0) * 100.0)

#-[SIGNAL METHODS]--------------------------------------------------------------
