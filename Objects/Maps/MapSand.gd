# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends TileMap
tool

#-[CONSTANTS]---------------------------------------------------------------------

const SAND : int = 0
const WATER : int = 1
const NAVI : int = 2
const WALL : int = 3
const LOWSAND : int = 4

#-[SIGNALS]---------------------------------------------------------------------

signal navi_updated

#-[EXPORT]----------------------------------------------------------------------

export var clear : bool = false setget set_clear
export var update : bool = false setget set_update
export var splash_scene : PackedScene = null
export (Array, NodePath) var maps_decoration : Array = ["", "", ""]

#-[ONREADY]---------------------------------------------------------------------

onready var Water : TileMap = $Water
onready var SandUnderWater : TileMap = $SandUnderWater
onready var NaviMap : TileMap = $Navi/Map
onready var Navi : Navigation2D = $Navi
onready var BG : Polygon2D = $BG

#-[VAR]-------------------------------------------------------------------------

var time : float = 0.0
var buildable : Dictionary = Dictionary()

var navi_dict_cells : Dictionary = Dictionary()
var navi_dict_ids : Dictionary = Dictionary()
var navi_blocked : Dictionary = Dictionary()
var navi : AStar2D = AStar2D.new()

#-[SETGET METHODS]--------------------------------------------------------------

func set_clear(__ : bool = false) -> void:
	if is_instance_valid(SandUnderWater):
		SandUnderWater.clear()
	if is_instance_valid(Water):
		Water.clear()
	if is_instance_valid(Navi):
		NaviMap.clear()


func set_update(__ : bool = false) -> void:
	set_clear()
	
	if is_instance_valid(SandUnderWater):
		for cell in get_used_cells_by_id(SAND):
			buildable[cell] = true
			fill_square(SandUnderWater, cell, SAND)
	
		if is_instance_valid(Water):
			var rect : Rect2 = get_used_rect()
			rect.position.x = min(rect.position.x, -20)
			rect.position.y = min(rect.position.y, -20)
			rect.size.x = max(rect.size.x, 60)
			rect.size.y = max(rect.size.y, 44)
			for x in range(rect.position.x - 4, rect.end.x + 4):
				for y in range(rect.position.y - 4, rect.end.y + 4):
					Water.set_cell(x, y, WATER)
					
			if is_instance_valid(Navi):
				for cell in Water.get_used_cells():
					if SandUnderWater.get_cellv(cell) == INVALID_CELL:
						NaviMap.set_cellv(cell, NAVI)
						navi_dict_cells[cell] = null
						#navi.add_point(navi.get_available_point_id(), cell)
		
		for cell in get_used_cells_by_id(LOWSAND):
			if not Engine.editor_hint:
				set_cellv(cell, INVALID_CELL)
			buildable[cell] = true
			fill_square(SandUnderWater, cell, SAND)
			
		SandUnderWater.update_bitmask_region()
		for cell in navi_dict_cells.keys():
			navi_dict_cells[cell] = navi.get_available_point_id()
			navi_dict_ids[navi_dict_cells[cell]] = cell
			navi.add_point(navi_dict_cells[cell], cell)

		for cell in navi_dict_cells.keys():
			var directions : PoolVector2Array = [
				cell + Vector2.RIGHT + Vector2.UP,
				cell + Vector2.RIGHT,
				cell + Vector2.RIGHT + Vector2.DOWN,
				cell + Vector2.DOWN
			]
			for direction in directions:
				if direction in navi_dict_cells:
					navi.connect_points(navi_dict_cells[cell], navi_dict_cells[direction])
		var poly_rect : Rect2 = Water.get_used_rect()
		poly_rect.position.x = min(poly_rect.position.x, -20)
		poly_rect.position.y = min(poly_rect.position.y, -20)
		poly_rect.size.x = max(poly_rect.size.x, 60)
		poly_rect.size.y = max(poly_rect.size.y, 44)
		poly_rect.position *= 32.0
		poly_rect.size *= 32.0
		BG.polygon[0] = poly_rect.position
		BG.polygon[1].x = poly_rect.end.x
		BG.polygon[1].y = poly_rect.position.y
		BG.polygon[2] = poly_rect.end
		BG.polygon[3].x = poly_rect.position.x
		BG.polygon[3].y = poly_rect.end.y
		

#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	assert(splash_scene, "No splash-scene")
	set_update()


func _process(delta : float) -> void:
	if Engine.editor_hint: return
	time += delta
	var x : float = time * 32.0 + sin(time) * 16.0
	var y : float = time * 16.0 + cos(time ) * 32.0
	x = int(fmod(x, 32.0))
	y = int(fmod(y, 32.0))
	Water.position = Vector2(x, y)


#func _draw() -> void:
#	for nav in navi_dict_cells.keys():
#		if navi_dict_cells[nav] in navi_blocked: continue
#		draw_circle(nav * 32.0, 4.0, Color(1.0, 0.0, 0.0, 0.1))
#		for con in navi.get_point_connections(navi_dict_cells[nav]):
#			if con in navi_blocked: continue
#			draw_line(nav * 32.0, navi_dict_ids[con] * 32.0, Color(1.0, 0.0, 0.0, 0.1))
#


#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func add_splash(gpos : Vector2) -> void:
	var new_splash : CPUParticles2D = splash_scene.instance()
	Water.add_child(new_splash)
	new_splash.original_position = gpos


func fill_square(map : TileMap, vec : Vector2, tile : int) -> void:
	map.set_cellv(vec, tile)
	map.set_cellv(vec + Vector2.RIGHT, tile)
	map.set_cellv(vec + Vector2.DOWN, tile)
	map.set_cellv(vec + Vector2.ONE, tile)


func get_navi_id_square(cell : Vector2) -> PoolIntArray:
	var output : PoolIntArray = PoolIntArray()
	for x in 2:
		for y in 2:
			if cell + Vector2(x, y) in navi_dict_cells:
				output.append(navi_dict_cells[cell + Vector2(x, y)])
	return output


func block_tile(cell : Vector2) -> void:
	if get_cellv(cell) == SAND: return
	fill_square(NaviMap, cell, INVALID_CELL)
	for id in get_navi_id_square(cell):
		navi_blocked[id] = navi_blocked.get(id, 0) + 1
		navi.set_point_disabled(id, true)
	update()
	emit_signal("navi_updated")


func unblock_tile(cell : Vector2) -> void:
	if get_cellv(cell) == SAND: return
	fill_square(NaviMap, cell, NAVI)
	for id in get_navi_id_square(cell):
		navi_blocked[id] = navi_blocked.get(id, 0) - 1
		if navi_blocked[id] <= 0:
			navi_blocked.erase(id) # warning-ignore:return_value_discarded
			navi.set_point_disabled(id, false)
	update()
	emit_signal("navi_updated")

#-[SIGNAL METHODS]--------------------------------------------------------------

