# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Area2D

#-[CONSTANTS]---------------------------------------------------------------------


const COLORS : Array = [
	Color.white,
	Color(1.0, 0.47, 0.0),
	Color(1.0, 0.82, 0.0)
]

#-[SIGNALS]---------------------------------------------------------------------



#-[EXPORT]----------------------------------------------------------------------



#-[ONREADY]---------------------------------------------------------------------

onready var points : Array = [
	get_node("1").get_children(),
	get_node("2").get_children(),
	get_node("3").get_children(),
	get_node("4").get_children(),
]

#-[VAR]-------------------------------------------------------------------------

var turrets : Array = Array()
var tier : int = 0 setget set_tier
var health : int = calc_fresh_health() 
var shoot_speed : float = 1.0
var shoot_range : float = 1.0
var money_spent : int = 0
var boosters : int = 0

#-[SETGET METHODS]--------------------------------------------------------------

func set_tier(t : int) -> void:
	tier = t
	$Visual.self_modulate = COLORS[t]

#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	pass


func _process(_delta : float) -> void:
	pass


func _physics_process(_delta : float) -> void:
	pass



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func add_turret(turret : PackedScene) -> void:
	assert(turrets.size() < 4, "Too many turrets!")
	turrets.append(turret.instance())
	add_child(turrets[-1])
	#print(points[turrets.size()])
	for i in turrets.size():
		turrets[i].position = points[turrets.size() - 1][i].position


func damage(_foo : float, _bar : float) -> void:
	if tier == 2: return
	health -= 1
	if health <= 0:
		$HealthReset.start()
		$Fire.emitting = true
		for turret in turrets:
			turret.can_fire = false


func upgrade(_shoot_speed : bool, _shoot_range : bool) -> void:
	self.tier += 1
	assert(tier <= 2, "too high tier!")
	if _shoot_range:
		shoot_range += 0.25
	if _shoot_speed:
		shoot_speed += 0.25
	for turret in turrets:
		turret.upgrade(shoot_range, shoot_speed)

func boost() -> void:
	boosters += 1
	for turret in turrets:
		turret.boost = float(boosters * boosters)


func calc_fresh_health() -> int:
	return (tier + 1) * 4

#-[SIGNAL METHODS]--------------------------------------------------------------


func _on_HealthReset_timeout() -> void:
	health = calc_fresh_health()
	$Fire.emitting = false
	for turret in turrets:
		turret.can_fire = true
