# +------------------------------------------------------+
# |                                                      |
# | Written by Benedikt Wicklein aka whiteshampoo - 2022 | 
# |                                                      |
# |               License: CC BY-NC-SA 4.0               |
# |  https://creativecommons.org/licenses/by-nc-sa/4.0/  |
# |                                                      |
# +------------------------------------------------------+


extends Area2D
tool

#-[CONSTANTS]---------------------------------------------------------------------

const PATH : String = "res://Objects/Units/Ships/Parts/"
const PATHS : Dictionary = {
	"Body": PATH + "Ship/Damage_%d.png",
	"SailWind": PATH + "SailSmall/%s_Damage_%d.png",
	"SailBig": PATH + "SailBig/%s_Damage_%d.png",
	"Flag": PATH + "Flag/%s.png"
}

#-[SIGNALS]---------------------------------------------------------------------

signal sunk(me)
signal exploding

#-[EXPORT]----------------------------------------------------------------------

export (String, "Black", "Blue", "Green", "Red", "Yellow") var type : String = "Black" setget set_type
export (float, 50.0, 500.0, 0.5) var max_health_body : float = 100.0
export (float, 0.0, 1.0, 0.01) var damage_body_1 : float =  0.66
export (float, 0.0, 1.0, 0.01) var damage_body_2 : float =  0.33
export (float, 1.0, 10.0, 0.5) var damage_body_resistance = 1.0
export (float, 50.0, 500.0, 0.5) var max_health_sail : float = 100.0
export (float, 0.0, 1.0, 0.01) var damage_sail_1 : float =  0.66
export (float, 0.0, 1.0, 0.01) var damage_sail_2 : float =  0.33
export (float, 1.0, 10.0, 0.5) var damage_sail_resistance = 1.0
export (float, 16.0, 64.0, 0.5) var speed : float = 64.0
export var gold : int = 20

#-[ONREADY]---------------------------------------------------------------------

onready var Body : Sprite = $Body
onready var SailWind : Sprite = $Body/SailWind
onready var SailBig : Sprite = $SailBig
onready var Flag : Sprite = $SailBig/Nest/Flag

onready var Navi : Navigation2D = get_node("../MapSand").Navi
onready var navi : AStar2D = get_node("../MapSand").navi

#-[VAR]-------------------------------------------------------------------------

var health_body : float = 0.0
var health_sail : float = 0.0

var direction : Vector2 = Vector2.ZERO

var sprites : Dictionary = {
	"Body": [null, null, null],
	"SailWind": [null, null, null],
	"SailBig": [null, null, null],
	"Flag": null
}

var target : Position2D = null
var path : PoolVector2Array = PoolVector2Array()

#-[SETGET METHODS]--------------------------------------------------------------

func set_type(t : String) -> void:
	type = t
	sprites_load()
	sprites_update()

#-[BUILTIN GODOT METHODS]-------------------------------------------------------

func _ready() -> void:
	health_body = max_health_body
	health_sail = max_health_sail
	sprites_load()
	sprites_update()
	var targets : Array = get_tree().get_nodes_in_group("Target")
	target = targets[randi() % targets.size()]
	call_deferred("navigation")
	get_tree().get_nodes_in_group("MapSand")[0].connect("navi_updated", self, "navigation")


func _physics_process(delta : float) -> void:
	if Engine.editor_hint: return
	if is_zero_approx(health_body): return
	var old_pos = position
	if path:
		if global_position.distance_to(path[0] * 32.0) <= 16.0:
			#navigation()
			path.remove(0)
		else:
			look_at(path[0] * 32.0)
		var sail_speed : float = (health_sail / max_health_sail + 1.0) / 2.0 * speed
		position += Vector2.RIGHT.rotated(rotation) * sail_speed * delta
	else:
		navigation()
	direction = position - old_pos
		



#-[INHERITED METHODS]-----------------------------------------------------------



#-[OWN METHODS]-----------------------------------------------------------------

func navigation() -> void:
	var next_point : int = navi.get_closest_point(global_position / 32.0)
	var target_point : int = navi.get_closest_point(target.global_position / 32.0)
	path = navi.get_point_path(next_point, target_point)
	if global_position.distance_to(target.global_position) <= 48.0:
		explode()


func sprites_load() -> void:
	for damage in 3:
		sprites["Body"][damage] = load(PATHS["Body"] % damage)
		sprites["SailWind"][damage] = load(PATHS["SailWind"] % [type, damage])
		sprites["SailBig"][damage] = load(PATHS["SailBig"] % [type, damage])
	sprites["Flag"] = load(PATHS["Flag"] % type)


func sprites_update() -> void:
	update_body()
	update_sail_wind()
	update_sail_big()
	update_flag()


func update_sprite(sprite : Sprite, texture : Texture) -> void:
	if not is_instance_valid(sprite): return
	sprite.texture = texture


func update_body() -> void:
	update_sprite(Body, sprites["Body"][calc_damage_body()])


func update_sail_wind() -> void:
	update_sprite(SailWind, sprites["SailWind"][calc_damage_sail()])


func update_sail_big() -> void:
	update_sprite(SailBig, sprites["SailBig"][calc_damage_sail()])


func update_flag() -> void:
	update_sprite(Flag, sprites["Flag"])


func calc_damage(max_health : float, health : float, damage_1 : float, damage_2 : float) -> int:
	var damage : float = health / max_health
	if damage < damage_2:
		return 2
	if damage < damage_1:
		return 1
	return 0


func calc_damage_body() -> int:
	return calc_damage(max_health_body, health_body, damage_body_1, damage_body_2)


func calc_damage_sail() -> int:
	return calc_damage(max_health_sail, health_sail, damage_sail_1, damage_sail_2)


func damage(damage_body : float, damage_sail : float) -> void:
	if health_body <= 0.0: return
	health_body -= damage_body / damage_body_resistance
	health_body = clamp(health_body, 0.0, max_health_body)
	health_sail -= damage_sail / damage_sail_resistance
	health_sail = clamp(health_sail, 0.0, max_health_sail)
	sprites_update()
	if is_zero_approx(health_body):
		$AnimationPlayer.play("sink")
		$Sinking.pitch_scale = rand_range(0.9, 1.1)
		$Sinking.play()
		get_tree().get_nodes_in_group("Overlay")[0].mod_gold(gold)


func explode() -> void:
	if health_body <= 0.0: return
	health_body = 0.0
	var E : CPUParticles2D = preload("res://Objects/Units/Ships/Explosion.tscn").instance()
	get_parent().add_child(E)
	E.emitting = true
	E.get_node("AnimationPlayer").play("explode")
	E.global_position = global_position
	E.global_rotation = global_rotation
	$AnimationPlayer.play("sink")
	emit_signal("exploding")
	$Sinking.pitch_scale = rand_range(0.9, 1.1)
	$Sinking.play()


func dead() -> void:
	emit_signal("sunk", self)
	queue_free()

#-[SIGNAL METHODS]--------------------------------------------------------------
