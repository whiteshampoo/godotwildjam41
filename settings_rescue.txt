{
"audio Master mute": false,
"audio Master volume": 0.5,
"audio Music mute": false,
"audio Music volume": 0.5,
"audio Sound mute": false,
"audio Sound volume": 0.5,
"color brightness": 1.0,
"color contrast": 1.0,
"color gamma": 1.0,
"color saturation": 1.0,
"fullscreen": false,
"lang_init": false,
"language": "en"
}